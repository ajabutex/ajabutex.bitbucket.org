% Vim user manual
% Ajabu Tex
% 2014

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-47717281-1', 'bitbucket.org');
  ga('send', 'pageview');

</script>

\| [Home](../index.html)
\| [Repository](https://bitbucket.org/ajabutex/vimscript-handbooks)
\| [Issues tracker](https://bitbucket.org/ajabutex/vimscript-handbooks/issues/new)
\|

******
<!-- -->

# Vim user manual

* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

Content of the repository
-------------------------

This is the markdown format of the [_Vim user manual_](http://vimdoc.sourceforge.net/htmldoc/usr_toc.html) by Bram Moolenaar.

I've converted the files that come with the _Vim 7.4_ installation (Linux version) inside the folder `/usr/share/vim/vim74/doc/`.


How to use it
-------------

You need [git](http://git-scm.com/),
[pandoc](http://johnmacfarlane.net/pandoc/) and a [LaTeX
installation](http://www.tug.org/texlive/)

Clone the repo on your hd with the following statement:

```bash
$ git clone https://ajabutex@bitbucket.org/ajabutex/vimscript-handbooks.git
```

then run in a terminal the bash script that comes in the "b.moolenaar"
directory:

```bash
$ ./makepdf
```

The bash script join all the chapters into one single document and
compile it first in LaTeX and then in pdf. `makepdf` uses `ajt.latex`
which is a pandoc template aimed to optimize the compilation in LaTeX of
the markdown documents. You can change it if you know what you do.

Contributing
------------

[Fork](https://bitbucket.org/ajabutex/vimscript-handbooks/fork) and
[pull
requests](https://bitbucket.org/ajabutex/vimscript-handbooks/pull-request/new)
are welcome, so as all [error
reports](https://bitbucket.org/ajabutex/vimscript-handbooks/issues/new).

