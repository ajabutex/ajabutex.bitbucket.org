% Vimscript Handbooks home page
% Ajabu Tex
% 2014

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-47717281-1', 'bitbucket.org');
  ga('send', 'pageview');

</script>

<!-- Navigation bar - Delete it from the README version -->
\| [Home](../index.html)
\| [Repository](https://bitbucket.org/ajabutex/vimscript-handbooks)
\| [Issues tracker](https://bitbucket.org/ajabutex/vimscript-handbooks/issues/new)
\|

******
<!-- -->
Vimscript handbooks
===================

in [_Pandoc's_
markdown](http://johnmacfarlane.net/pandoc/demo/example9/pandocs-markdown.html)
format.

* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *


Content of the repository
-------------------------

At the moment there are the following handbooks, one for every folder:

**d.conway**
:	Dr. Damian Conway\
	_Scripting the Vim editor_\
	6 May 2009 - 3 March 2010\

**s.losh**
:	Steve Losh\
	[_Learn Vimscript the hard way_](lvthw.html)\
	4 April 2013\

**b.moolenaar**
:   Bram Moolenaar\
    [_Vim user manual_](vum.html)\
    20 February 2013\

**swaroop**
:   Swaroop C. H.\
    _A byte of Vim_\
    25 November 2008\

**clearmoments**
:   Clearmoments on vim.wikia.com\
    _Creating your own syntax files_\
    2008\

**i.karkat**
:   Ingo Karkat on vim.wikia.com\
    _Syntax folding of Vim scripts_\
    2006


How to use it
-------------

Clone the repo on your hd with the following statement:

```bash
git clone https://ajabutex@bitbucket.org/ajabutex/vimscript-handbooks.git
```

you need [Git](http://git-scm.com/) to do that.

To compile the documents in LaTeX or directly PDF use the bash scripts
that came into the same directories. You need
[_Pandoc_](http://johnmacfarlane.net/pandoc/) by John MacFarlane.

`ajt.latex` is a template for _Pandoc_ that optimizes
the compilation.


Contributing
------------

[Fork](https://bitbucket.org/ajabutex/vimscript-handbooks/fork) and
[pull
requests](https://bitbucket.org/ajabutex/vimscript-handbooks/pull-request/new)
are welcome, so as all [error
reports](https://bitbucket.org/ajabutex/vimscript-handbooks/issues/new).

