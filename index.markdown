% Projects home page
% Ajabu Tex
% 2014

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-47717281-1', 'bitbucket.org');
  ga('send', 'pageview');
</script>

Ajabu Tex projects
==================

* * * *

Actually are hosted the following projects:

[TrY](try/index.html)
:   A tool that automates the compilation of TeX/LaTeX documents.

[Vim _advanced_ reference card](vrc/index.html)
:   Yes, yet another Vim reference card. I'm writing it as a personal
    training job aimed to learn the Vim commands. It's written in plain
    TeX with an it's own format.

[Vimscript handbooks](vhb/index.html)
:   A series of well-known documents about Vim and Vimscript in pandoc's
    markdown format. They comes with a LaTeX template for pandoc and a
    series of bash script with wich it is possible to compile every
    document in LaTeX or PDF.

More to come.

| Ajabu Tex
| <ajabutex@gmail.com>
