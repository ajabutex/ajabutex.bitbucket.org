% Vim advanced reference card home page
% Ajabu Tex
% 2014

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-47717281-1', 'bitbucket.org');
  ga('send', 'pageview');

</script>

<!-- Navigation bar. Delete it from the README version -->
\| [Home](../index.html)
\| [Repository](https://bitbucket.org/ajabutex/vim-reference)
\| [Issues tracker](https://bitbucket.org/ajabutex/vim-reference/issues/new)
\|

******
<!-- End of the navigation bar -->

VIM _ADVANCED_ REFERENCE CARD
=============================

A Vim reference card, largely inspired by the [Vim Quick Reference
Card](http://tnerual.eriogerg.free.fr/vim.html) by Laurent Gregoire.

This first stage of the project follows the structure of the "Vim user
manual".

[Fork](https://bitbucket.org/ajabutex/vim-reference/fork), [pull
requests](https://bitbucket.org/ajabutex/vim-reference/pull-request/new),
or [error
reports](https://bitbucket.org/ajabutex/vim-reference/issues/new) are
welcome.
