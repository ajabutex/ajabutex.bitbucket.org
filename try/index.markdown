% TrY home page
% Ajabu Tex
% 2014

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-47717281-1', 'bitbucket.org');
  ga('send', 'pageview');

</script>

<!-- Delete from the README version -->
\| [Home](../index.html)
\| [Repository](https://bitbucket.org/ajabutex/try)
\| [Issues tracker](https://bitbucket.org/ajabutex/try/issues/new)
\|

******
<!-- Till here -->

TrY
===

Automation of the TeX/LaTeX compilation.

Author: Ajabu Tex <ajabutex@gmail.com>  
Version: 4.0 - Release 2013-2014

******

Program for the automation of the compilation of (La)TeX documents.
Scans the commands from the comment lines of the document and executes
them automatically.

Great attention is paid to the flexibility and ease of use.


Limitations
-----------

TrY is written for Linux and works on Linux. I didn't test the program
on Windows.\
**CAUTION:** I don't know if the program properly works on
Windows.  **I think not.**


Content of the package
----------------------

* try.nw: the source code for the program and the documentation
* try: the python program
* try-doc.pdf: the documentation
* README: this file
* COPYING: GNU GPL license informations


How to compile it
-----------------

The source file (`try.nw`) is written in [noweb][*], a tool by
Norman Ramsey for [_Literate Programming_][**]. To extract and
compile the program and the documentation use the following commands:

program

```bash
$ notangle -Rtry try.nw > try
$ chmod +x try
```

documentation

```bash
$ noweave -index -latex try.nw > try-doc.tex
$ pdflatex try-doc.tex
$ pdflatex try-doc.tex
$ pdflatex try-doc.tex
```


Download
--------

The latest version of the program is 4.0.\
[Here](https://bitbucket.org/ajabutex/try/downloads/TrY40.rar) you can
download the noweb source file, the python script and all the
documentation already compiled in PDF format.


Installation
------------

Make the file `try` executable then copy it into the `bin` directory of
your system, e.g.:

```bash
$ chmod +x try 
$ sudo cp try /usr/bin/try
```


## Use ##

Put the instructions for the compilation of the TeX document into its
comment lines, e.g.:

```latex
%
%$ xelatex -shell-escape mydoc.tex
%$ bibtex -min-crossref=3 mydoc.aux
%$ xelatex -shell-escape mydoc.tex
%$ xelatex -shell-escape mydoc.tex
%
```

Now you can make the full cycle of the compilation of the document
running a single command in a terminal:

```bash
$ try mydoc.tex
```

There are some command line options. More on this in
[try-doc.pdf](https://bitbucket.org/ajabutex/try/downloads/TrY40.rar).


Contributing
------------

If you want to [fork the
project](https://bitbucket.org/ajabutex/try/fork) or if you want to send
a [pull request](https://bitbucket.org/ajabutex/try/pull-request/new),
or simply [report an
error](https://bitbucket.org/ajabutex/try/issues/new), you are welcome.
    

License
-------

This material is subject to the GPL - GNU General Public License. See

<http://www.gnu.org/licenses/gpl.txt>

for the details of that license.


Happy TeXing.

[*]: http://www.cs.tufts.edu/~nr/noweb/
[**]: http://www.literateprogramming.com/
